import java.io.*;
public class OpenSaveFile
{
	public static void main(String[] args) throws IOException
	{
		try
		{
			//创建文件输入流
			FileInputStream in=new FileInputStream("OpenSaveFile.java");
			//创建输出流
			FileOutputStream out=new FileOutputStream("OpenSaveFile.bak");
			int c;
			while ((c=in.read())!=-1)
			{
				out.write(c);
			}
			System.out.println("操作成功，在同目录下生成了OpenSaveFile.bak");
			in.close();
			out.close();
		}
		catch (IOException ioe)
		{
			System.out.println(ioe);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}
}