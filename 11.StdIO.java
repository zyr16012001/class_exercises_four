import java.io.*;
public class StdIO
{
	public static void main(String[] args) throws IOException
	{
		System.out.println("Please input a string:");
		byte buffer[]=new byte[512];
		int count=System.in.read(buffer);  //从缓冲区读数据
		System.out.println("Output your string:");
		for (int i=0;i<count ;i++ )
		{
			System.out.print((char)buffer[i]);           
		}
		System.out.println("Count:"+count);
	}
}