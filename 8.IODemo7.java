import java.io.*;
public class IODemo7
{
	public static void main(String[] args)
	{
		String s=new String();
		float f=3.145f;
		try
		{
			//part1
			RandomAccessFile rwFile1=new RandomAccessFile("rwtest.txt","rw");
			rwFile1.writeFloat(3.14f);
			rwFile1.writeFloat(4.14f);
			rwFile1.writeFloat(5.14f);
			rwFile1.writeFloat(10.14f);
			rwFile1.writeFloat(9.14f);
			rwFile1.close();
			//part2
			RandomAccessFile rFile1=new RandomAccessFile("rwtest.txt","r");
			System.out.println("the first time when written");
			for(int i=0;i<5;i++)
				System.out.println("Value "+i+ " : "+rFile1.readFloat());
			rFile1.close();
			//part3
			RandomAccessFile rwFile2=new RandomAccessFile("rwtest.txt","rw");
			rwFile2.seek(8);
			System.out.println("pos1="+rwFile2.getFilePointer());
			rwFile2.writeFloat(0f);
			System.out.println("pos2="+rwFile2.getFilePointer());
			rwFile2.close();
			//part4
			RandomAccessFile rFile2=new RandomAccessFile("rwtest.txt","r");
			System.out.println("the second time when written");
			for(int i=0;i<5;i++)
				System.out.println("Value "+i+" :"+rFile2.readFloat());
			//part5
			System.out.println("pos3="+rFile2.getFilePointer());
			System.out.println("length="+rFile2.length());
			rFile2.close();
		}
		catch (Exception e)
		{
		}
	}
}