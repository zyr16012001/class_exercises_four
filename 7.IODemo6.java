import java.io.*;
class IODemo6
{
	public static void main(String[] args)
	{
		try
		{
			FileInputStream in=new FileInputStream("source.txt");
			System.setIn(in);
			PrintStream out=new PrintStream(new FileOutputStream("mytest.txt"));
			System.setOut(out);
			System.setErr(out);
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			String s;
			while ((s=br.readLine())!=null)
			{
				System.out.println(s);
			}
			in.close();
			out.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}