import java.io.*;
public class FileClass1
{
	public static void main(String[] args)
	{
		File path=new File("d:\\javacode\\test");
		File oldFile=new File("d:\\javacode\\test\\My.class");
		File newPath=new File("d:\\javacode\\test\\tmp");
		File newFile=new File("d:\\javacode\\test\\tmp\\Ok.ok");

		String[] stringArray;
		stringArray=path.list();
		System.out.println("begin list the file names in path");
		for(int i=0;i<stringArray.length;i++)
			System.out.println(stringArray[i]);
		System.out.println("name of path="+path.getName());
		System.out.println("parent of path="+path.getParent());
		System.out.println("length of oldFile="+oldFile.length());
		newPath.mkdir();
		try
		{
			newFile.createNewFile();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		stringArray=newPath.list();
		System.out.println("begin list the file names in newpath");
		for(int i=0;i<stringArray.length;i++)
			System.out.println(stringArray[i]);
		System.out.println("absolute pathname of newfile="+newFile.getAbsolutePath());
		System.out.println("length of newFile="+newFile.length());
		try
		{
			System.in.read();
		}
		catch (Exception e)
		{
		}
	}
}