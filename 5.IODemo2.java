import java.io.*;
public class IODemo2
{	
	public static void main(String[] args)
	{
		int i;
		byte b;
		try
		{
			DataInputStream in=new DataInputStream(new FileInputStream("source.txt"));
			i=0;
			while(i>=0)
			{
				try
				{
					b=in.readByte();
					System.out.println((char)b);
					i++;
				}
				catch (Exception e)
				{	
					System.out.println();
					System.out.println("TOTAL="+i);
					in.close();
					i=-1;
				}

			}
		}
		catch (Exception e)
		{
		}
		try
		{
			System.in.read();
		}
		catch (Exception e)
		{
		}
	}
}