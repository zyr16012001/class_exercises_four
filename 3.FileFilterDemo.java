import java.io.*;
public class FileFilterDemo implements FilenameFilter
{
	private String prefix="",suffix=""; //文件名前、后缀
	public FileFilterDemo(String filterstr)
	{
		filterstr=filterstr.toLowerCase();
		int i=filterstr.indexOf('*');           //获取'*'的索引值
		int j=filterstr.indexOf('.');
		if(i>0)
			prefix=filterstr.substring(0,i);
		if(j>0)
			suffix=filterstr.substring(j+1);
	}
	public static void main(String[] args)
	{
		//创建带通配符的文件名过滤器对象
		FilenameFilter filter=new FileFilterDemo("*.java");
		File f1=new File("");
		File curdir=new File(f1.getAbsolutePath(),"");  //当前目录
		File listFile=new File("javalist","listFile.txt");
		try
		{
			FileOutputStream wf=new FileOutputStream(listFile);
			byte bf[]=new byte[256];
			System.out.println(curdir.getAbsolutePath());
			String[] str=curdir.list(filter);  //列出带过滤器的文件名清单
			for (int i=0;i<str.length ;i++ )
			{
				System.out.println("\t"+str[i]);
				bf=str[i].getBytes();
				wf.write(bf);
				wf.write('\n');
			}
			wf.close();
		}
		catch (FileNotFoundException nfe)
		{
			System.out.println(nfe.toString());
		}
		catch(IOException e)
		{
			System.out.println(e.toString());
		}
	}
	public boolean accept(File dir,String filename)
	{
		boolean bAccept=true;
		try
		{
			filename=filename.toLowerCase();
			bAccept=(filename.startsWith(prefix))&(filename.endsWith(suffix));

		}
		catch (NullPointerException e)
		{
		}
		return bAccept;
	}
}